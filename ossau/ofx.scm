
;; Reading transactions from a Nationwide OFX file.

(define-module (ossau ofx)
  #:use-module (ice-9 format)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 string-fun)
  #:use-module (ice-9 textual-ports)
  #:use-module (ossau fold-input)
  #:use-module (sxml match)
  #:use-module (sxml simple)
  #:export (format-2dp
	    get-transactions
	    get-transactions-txt
	    tx:date
	    tx:type
	    tx:description
	    tx:amount))

;; Format an AMOUNT of money as a string with 2 decimal places.
;; AMOUNT can be a string or a number."
(define (format-2dp amount)
  (format #f "~,2f" (cond ((number? amount) amount)
			  ((string? amount) (string->number amount))
			  (else (error "AMOUNT should be a number or a string")))))

;; Read and returns the list of transactions from OFXFILE.  Each
;; transaction is represented as (DATE TYPE DESCRIPTION AMOUNT), with
;; all elements being strings.  AMOUNT is positive for income and
;; negative for expenses.
(define (get-transactions ofxfile)
  (let loop ((data (get-tag-value (xml->sxml (get-string-all (open-input-file ofxfile)))
				  'OFX
				  'BANKMSGSRSV1
				  'STMTTRNRS
				  'STMTRS
				  'BANKTRANLIST))
	     (transactions '()))
    (cond ((null? data)
	   (reverse transactions))
	  ((eq? (caar data) 'STMTTRN)
	   (loop (cdr data)
		 (cons (sxml-match (car data)
		         [(STMTTRN (TRNTYPE ,type) (DTPOSTED ,date) (TRNAMT ,amount) (FITID ,fitid) (NAME ,name))
			  (list date type name (format-2dp amount))])
		       transactions)))
	  (else
	   (loop (cdr data) transactions)))))

;; Read transactions in ad-hoc text format, with:
;; - one transaction per line
;; - format "YYYY-MM-DD:DESCRIPTION:AMOUNT:", with final ":" sometimes
;;   absent, and with no ":" allowed in DESCRIPTION
;; - AMOUNT is positive for an expense and negative for income.
(define (get-transactions-txt txtfile)
  (with-input-from-file txtfile
    (lambda ()
      (reverse (fold-input (lambda (line acc)
			     (cons (separate-fields-discarding-char
				    #\:
				    line
				    (lambda (date desc amount . rest)
				      (list date
					    "?"
					    desc
					    (if (string-prefix? "-" amount)
						(substring amount 1)
						(string-append "-" amount)))))
				   acc))
			   '()
			   read-line)))))

(define (get-tag-value x . tags)
  (let loop ((x x) (tags tags))
    (cond ((null? tags)
	   x)
	  ((null? x)
	   (error "Unexpected end of data"))
	  ((and (pair? (car x)) (eq? (caar x) (car tags)))
	   (loop (cdar x) (cdr tags)))
	  (else
	   (loop (cdr x) tags)))))

(define tx:date car)
(define tx:type cadr)
(define tx:description caddr)
(define tx:amount cadddr)
